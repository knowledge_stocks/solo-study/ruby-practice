# routes.rb 이곳에서 특정 경로와 컨트롤러를 매핑한다.

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
	
	root 'home#index' # root 경로를 home의 index 액션과 매핑
	# get root 'home#index' # root 경로를 home의 index 액션에 get 메소드로 매핑
	# get '/' => 'home#index' # 위와 동일
	
	get '/menu' => 'home#menu'
end
